source 'https://rubygems.org'

gem 'rails', '~> 5.0.0', '>= 5.0.0.1'

gem 'pg', '~> 0.18'
gem 'puma', '~> 3.0'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'therubyracer', platforms: :ruby
gem 'jquery-rails'
gem 'jbuilder', '~> 2.5'

group :development, :test do
  # Plays well with guard console. Type 'byebug' anywhere to debug.
  gem 'byebug', platform: :mri
end

group :development do
  gem 'web-console'
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  # Ruby style guide code analyzer
  # https://rubocop.readthedocs.io
  gem 'rubocop', require: false

  # A static analysis security vulnerability scanner for Ruby on Rails applications
  # https://github.com/presidentbeef/brakeman
  gem 'brakeman', :require => false

  # Chrome extension for Rails Development
  # https://github.com/dejan/rails_panel/
  gem 'meta_request'

  # Intelligently launch tests when files are modified
  # https://github.com/guard/guard
  # https://github.com/guard/guard-minitest
  # https://github.com/yujinakayama/guard-rubocop
  # https://github.com/guard/guard-brakeman
  gem 'guard'
  gem 'guard-minitest'
  gem 'guard-rubocop'
  gem 'guard-brakeman'

  # Better error pages.
  # https://github.com/charliesome/better_errors/
  gem 'better_errors'
  gem 'binding_of_caller'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
